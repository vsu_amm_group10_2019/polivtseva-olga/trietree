﻿using System;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class MainForm : Form
    {
        private readonly TrieTree tree = new TrieTree();

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (CheckWord() == false)
                return;
            if (tree.Add(tbWord.Text) == false)
            {
                MessageBox.Show($"{tbWord.Text} не было добавлено", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            tbWord.Text = "";
            RefreshWords();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (CheckWord() == false)
                return;
            if (tree.Delete(tbWord.Text) == false)
            {
                MessageBox.Show($"{tbWord.Text} не было удалено", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            tbWord.Text = "";
            RefreshWords();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            if (CheckWord() == false)
                return;
            if (tree.Find(tbWord.Text))            
                MessageBox.Show($"{tbWord.Text} найдено", "Успех", MessageBoxButtons.OK);            
            else            
                MessageBox.Show($"{tbWord.Text} не найдено", "Неудача", MessageBoxButtons.OK);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tree.Clear();
            RefreshWords();
        }

        private void DeleteEvenBtn_Click(object sender, EventArgs e)
        {
            tree.DeleteEven();
            RefreshWords();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
                return;

            System.IO.File.WriteAllText(saveFileDialog.FileName, tree.ToString());
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;

            tree.Clear();
            string fileText = System.IO.File.ReadAllText(openFileDialog.FileName);
            char[] s = { '\n' };
            string[] words = fileText.Split(s, StringSplitOptions.RemoveEmptyEntries);
            foreach (string word in words)
            {
                tree.Add(word.Trim());
            }
            RefreshWords();
        }

        private bool CheckWord()
        {
            if (string.IsNullOrWhiteSpace(tbWord.Text))
            {
                MessageBox.Show("Поле не содержит слов", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void RefreshWords()
        {
            FillTreeView();
        }

        private void FillTreeView()
        {
            treeView.Nodes.Clear();
            AddChilds(treeView.Nodes, tree.Root);
            treeView.ExpandAll();
        }

        private void AddChilds(TreeNodeCollection nodes, TreeNode trieNode)
        {
            if (trieNode.Childs.Count == 0)
                return;

            foreach (var keyValuePair in trieNode.Childs)
            {
                var newNode = nodes.Add(keyValuePair.Key.ToString());
                if (keyValuePair.Value.IsWord)
                    newNode.ForeColor = System.Drawing.Color.Green;
                AddChilds(newNode.Nodes, keyValuePair.Value);
            }
        }

       
    }
}
