﻿namespace TrieTree
{    
    public class TrieTree
    {
        public TreeNode Root { get; private set; }

        public TrieTree()
        {
            Root = new TreeNode();
        }

        public static bool CheckWord(string word)
        {
            return string.IsNullOrWhiteSpace(word) == false && word.Trim().Contains(' ') == false;
        }

        public bool Add(string word)
        {
            return CheckWord(word) && Root.AddWord(word.Trim());
        }

        public bool Find(string word)
        {
            return CheckWord(word) && IsEmpty() == false && Root.Find(word.Trim());
        }

        public bool Delete(string word)
        {
            return CheckWord(word) && IsEmpty() == false && Root.Delete(word.Trim());
        }

        public bool IsEmpty()
        {
            return Root.IsEmpty();
        }

        public void Clear()
        {
            Root = new TreeNode();
        }

        public void DeleteEven()
        {
            Root.DeleteAllEven(true);
        }

        public override string ToString()
        {
            string str = "";
            Root.GetString(ref str);
            return str;
        }
    }
}