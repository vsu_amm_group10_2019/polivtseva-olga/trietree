﻿using System;
using System.Collections.Generic;

namespace TrieTree
{    
    public class TreeNode
    {
        public Dictionary<char, TreeNode> Childs { get; private set; }
        public bool IsWord { get; private set; }
        public char Value { get; private set; }

        public TreeNode()
        {
            IsWord = false;
            Childs = new Dictionary<char, TreeNode>();
        }

        public TreeNode(char value)
        {
            this.Value = value;
            IsWord = false;
            Childs = new Dictionary<char, TreeNode>();
        }

      

        private TreeNode AddChild(char value)
        {
            var child = new TreeNode(value);
            Childs.Add(value, child);
            return child;
        }

        public bool AddWord(string word)
        {
            if (word.Length == 0)
            {
                if (IsWord)
                    return false;

                IsWord = true;
                return true;
            }

            if (Childs.TryGetValue(word[0], out var child) == false)
                child = AddChild(word[0]);
            return child.AddWord(word.Substring(1));
        }

        public bool Delete(string word)
        {
            if (word.Length == 0)
            {
                if (IsWord == false)
                    return false;

                IsWord = false;
                return true;
            }

            if (Childs.TryGetValue(word[0], out var child) == false)
                return false;
            return child.Delete(word.Substring(1));
        }

        public bool Find(string word)
        {
            if (word.Length == 0)
                return IsWord;


            var found = Childs.TryGetValue(word[0], out var child);
            return found && child.Find(word.Substring(1));
        }

        public bool IsEmpty()
        {
            return Childs.Count == 0;
        }

        public void GetString(ref string str, string word = "")
        {
            if (IsWord)
                str += word + Environment.NewLine;

            foreach (var child in Childs.Values)
                child.GetString(ref str, word + child.Value);
        }

        public void DeleteAllEven(bool even)
        {
            IsWord = IsWord && !even;

            foreach (var child in Childs.Values)
                child.DeleteAllEven(!even);
        }
    }
}
